#!/usr/bin/python
"""
Helper script to activate a NetworkManager connection even when the particular wifi has not been found yet.

Taken from https://github.com/seveas/python-networkmanager
Modified for eyefi-server
"""
import sys
try:
    import NetworkManager
except Exception, e:
    sys.exit("Please install python-networkmanager. For instructions see https://github.com/seveas/python-networkmanager")

# Find the connection
try:
    name = sys.argv[1]
except IndexError, e:
    sys.exit("Helper script to activate a NetworkManager connection even when the particular wifi has not been found yet. Useful to trigger the NetworkManager to wait for the Eye-Fi direct connection wifi.\nUsage: %s 'wifi-name'" % sys.argv[0])

connections = NetworkManager.Settings.ListConnections()
connections = dict([(x.GetSettings()['connection']['id'], x) for x in connections])
conn = connections[name]

# Find a suitable device
ctype = conn.GetSettings()['connection']['type']
if ctype == 'vpn':
    for dev in NetworkManager.NetworkManager.GetDevices():
        if dev.State == NetworkManager.NM_DEVICE_STATE_ACTIVATED and dev.Managed:
            break
    else:
        print("No active, managed device found")
        sys.exit(1)
else:
    dtype = {
        '802-11-wireless': NetworkManager.NM_DEVICE_TYPE_WIFI,
        '802-3-ethernet': NetworkManager.NM_DEVICE_TYPE_ETHERNET,
        'gsm': NetworkManager.NM_DEVICE_TYPE_MODEM,
    }.get(ctype,ctype)
    devices = NetworkManager.NetworkManager.GetDevices()
    
    for dev in devices:
        if dev.DeviceType == dtype:
            break
    else:
        print("No suitable and available %s device found" % ctype)
        sys.exit(1)

# And connect
NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")
