#!/bin/bash
#
# This is an example for a pre-upload command for the eyefi-server.
# It shows a notification popup.
# This can only work if the server runs with the same user as the desktop
# user and if the DISPLAY variable is set.
#
# To enable, set the following option in the config file:
# command_before_upload:/PATH-TO-THIS-FILE/eyefi-before-upload.sh

notify-send -u critical "Eye-Fi upload" "Receiving new image..."
