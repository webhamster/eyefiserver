#!/bin/bash
#
# This is an example for a post-upload command for the eyefi-server.
# It opens the last uploaded image in the gnome picture viewer.
# This can only work if the server runs with the same user as the desktop
# user and if the DISPLAY variable is set.
#
# To enable, set the following option in the config file:
# command_after_upload:/PATH-TO-THIS-FILE/eyefi-after-upload.sh

eog -w $1
